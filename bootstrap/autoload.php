<?php

use Illuminate\Workbench\Starter;

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Register The Workbench Starter
|--------------------------------------------------------------------------
|
| This will go through the workbench and load any projects that contain
| an autoload file. Reverts back the the L4 way of doing package creation,
| but for right now, it's what I'm going to use.`
|
*/
Starter::start( __DIR__ . '/../workbench');

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

if (file_exists($compiledPath = __DIR__.'/../vendor/compiled.php'))
{
	require $compiledPath;
}
elseif (file_exists($compiledPath = __DIR__.'/../storage/framework/compiled.php'))
{
	require $compiledPath;
}
