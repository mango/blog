## Mango

MMango is a very simple, extensible blogging system built on top of the Laravel 5 PHP framework. It is a testing grounds
of sorts.`

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
